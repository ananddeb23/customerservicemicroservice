'use strict';
module.exports = (sequelize, DataTypes) => {
  var customerData = sequelize.define('customerData', {
    cif: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.BIGINT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return customerData;
};