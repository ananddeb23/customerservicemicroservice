'use strict';
module.exports = (sequelize, DataTypes) => {
  var customerDetails = sequelize.define('customerDetails', {
    cif: DataTypes.STRING,
    address: DataTypes.STRING,
    phone: DataTypes.BIGINT
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return customerDetails;
};