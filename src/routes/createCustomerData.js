const express = require('express');
const Models = require('../../models');
const logger = require('../trace/logger')

const router = express.Router();

/* GET users listing. */
router.post('/', async (req, res) => {
  const uniq = (new Date().getUTCMilliseconds()) + Math.random().toString(36);
  const cif = uniq.substr(10);
  const cData = { cif, address: req.body.address, phone: req.body.phone };
  Models.customerDetails.create(cData).then((result) => {
    res.status(201);
    return res.send({
      status: 'Record created',
      cif: result.dataValues.cif
    });
  }).catch((err) => {
    logger.log(err.message);
  });
});

module.exports = router;
