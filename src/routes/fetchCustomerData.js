const express = require('express');
const Models = require('../../models');
const logger = require('../trace/logger')

const router = express.Router();

router.get('/:cif', async (req, res) => {
  const cif = req.params.cif;
  if (cif) {
    return Models.customerDetails.findOne({
      where: { cif },
    }).then((cData) => {
      if (cData !== null) {
        res.status(200);
        return res.send(cData.dataValues);
      }
      res.status(404);
      return res.send('Customer details not found');
    }).catch((err) => {
      logger.log(err.message);
    });
  }
  res.status(400);
  return res.send('Missing parameter');
});

module.exports = router;
